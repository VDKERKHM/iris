package be.mvdk.iris.app.model.command;

import org.springframework.util.StringUtils;

/**
 * Created by mvandenk on 6/10/2016.
 */
public enum Verb {

    ACTIVATOR,
    OPEN,
    SEARCH,
    SELECT,
    EXIT;


    public static Verb get(String verbString) {

        if (!StringUtils.hasText(verbString)) {
            return null;
        }

        for (Verb verb : values()) {
            if (verb.name().equals(verbString.toUpperCase())) {
                return verb;
            }
        }

        return null;

    }

}
