package be.mvdk.iris.app.model;

import be.mvdk.iris.app.model.command.Verb;
import be.mvdk.iris.app.model.command.VoiceCommand;

/**
 * Created by mvandenk on 6/10/2016.
 */
public class VoiceCommandHandler {

    public void handleVoiceCommand(final VoiceCommand command) {

        if (Verb.EXIT == command.getVerb()) {
            System.exit(0);
        }

        try {
            command.setIgnored(false);
            ExecutorFactory
                    .getExecutorByApplication(command.getApplication())
                    .executeVoiceCommand(command);
        } catch (Exception e) {
            System.out.println(e);
        }


    }
}
