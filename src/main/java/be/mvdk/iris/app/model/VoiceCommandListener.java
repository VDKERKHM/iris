package be.mvdk.iris.app.model;

import be.mvdk.iris.app.model.command.VoiceCommand;
import be.mvdk.iris.app.util.VoiceCommandUtil;
import be.mvdk.iris.app.views.View;
import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by mvandenk on 6/10/2016.
 */
public class VoiceCommandListener implements Runnable {


    private static final String ACOUSTIC_MODEL = "resource:/edu/cmu/sphinx/models/en-us/en-us";
    private static final String DICTIONARY_PATH = "resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict";
    private static final String GRAMMAR_PATH = "resource:/be/mvdk/iris/app/dialog/";
    private VoiceCommandHandler voiceCommandHandler;
    private View view;

    public VoiceCommandListener(View view) {
        this.view = view;
        voiceCommandHandler = new VoiceCommandHandler();
    }

    public void run() {
        Configuration configuration = new Configuration();
        configuration.setAcousticModelPath(ACOUSTIC_MODEL);
        configuration.setDictionaryPath(DICTIONARY_PATH);
        configuration.setGrammarPath(GRAMMAR_PATH);
        configuration.setUseGrammar(true);
        configuration.setGrammarName("dialog");

        LiveSpeechRecognizer recognizer = null;

        try {
            recognizer = new LiveSpeechRecognizer(configuration);
        } catch (IOException e) {
            e.printStackTrace();
        }
        recognizer.startRecognition(true);


        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            System.out.println(result.getHypothesis());

            VoiceCommand command = VoiceCommandUtil.createVoiceCommand(result);

            if (command != null)  {
                if (!command.isIgnored()) {
                    voiceCommandHandler.handleVoiceCommand(command);
                }
                writeResult(command);
            }
        }

    }

    private void writeResult(VoiceCommand command) {
        view.addTextFeedback(command);
    }
}
