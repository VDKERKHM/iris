package be.mvdk.iris.app.model.executor;

import be.mvdk.iris.app.model.command.VoiceCommand;
import be.mvdk.iris.app.util.DesktopUtil;
import org.springframework.util.StringUtils;

/**
 * Created by mvandenk on 6/10/2016.
 */
public class GoogleVoiceCommandExecutor implements VoiceCommandExecutor {

    private final String GOOGLE_BASE_URL = "https://www.google.be/";
    private final String GOOGLE_SEARCH_URL = "?gws_rd=ssl#q=";


    public void executeVoiceCommand(VoiceCommand voiceCommand) {
        switch (voiceCommand.getVerb()) {
            case OPEN:
                open();
                break;
            case SEARCH:
                search(voiceCommand);
                break;
        }
    }


    private void open() {
        DesktopUtil.openBrowser(GOOGLE_BASE_URL);
    }

    private void search(VoiceCommand command) {
        if (!StringUtils.hasText(command.getCommandString())) {
            return;
        }

        DesktopUtil.openBrowser(GOOGLE_BASE_URL + GOOGLE_SEARCH_URL + DesktopUtil.encodeParameterString(command.getCommandString()));
    }
}
