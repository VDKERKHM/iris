package be.mvdk.iris.app.model.command;

import com.google.common.base.MoreObjects;

/**
 * Created by mvandenk on 6/10/2016.
 */
public class VoiceCommand {

    private Verb verb;
    private Application application;
    private String commandString;

    private boolean ignored;

    public VoiceCommand(Verb verb, Application application, String commandString) {
        this.verb = verb;
        this.application = application;
        this.commandString = commandString;
    }

    public VoiceCommand(String verb, String application, String commandString) {
        this(Verb.get(verb), Application.get(application), commandString);
    }


    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    public Verb getVerb() {
        return verb;
    }

    public Application getApplication() {
        return application;
    }

    public String getCommandString() {
        return commandString;
    }

    public boolean isIgnored() {
        return ignored;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("verb", verb)
                .add("application", application)
                .add("commandString", commandString)
                .add("ignored", ignored)
                .toString();
    }
}
