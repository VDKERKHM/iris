package be.mvdk.iris.app.model.executor;

import be.mvdk.iris.app.model.command.Verb;
import be.mvdk.iris.app.model.command.VoiceCommand;
import be.mvdk.iris.app.util.DesktopUtil;

import java.io.File;

/**
 * Created by mvandenk on 17/10/2016.
 */
public class OfficeVoiceCommandExecutor implements VoiceCommandExecutor {


    private static final String WORD_LOCATION = "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\Word 2016.lnk";
    private static final String POWERPOINT_LOCATION = "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\PowerPoint 2016.lnk";

    @Override
    public void executeVoiceCommand(VoiceCommand voiceCommand) {
        if (Verb.OPEN == voiceCommand.getVerb()) {

            switch (voiceCommand.getApplication()) {
                case WORD:
                    open(WORD_LOCATION);
                    break;
                case POWERPOINT:
                    open(POWERPOINT_LOCATION);
                    break;
            }


        }
    }

    private void open(String application) {
        DesktopUtil.openFile(new File(application));
    }
}
