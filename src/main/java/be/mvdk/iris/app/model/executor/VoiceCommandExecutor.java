package be.mvdk.iris.app.model.executor;

import be.mvdk.iris.app.model.command.VoiceCommand;

/**
 * Created by mvandenk on 6/10/2016.
 */
public interface VoiceCommandExecutor {
    void executeVoiceCommand(VoiceCommand voiceCommand);
}
