package be.mvdk.iris.app.model.executor;

import be.mvdk.iris.app.model.command.Verb;
import be.mvdk.iris.app.model.command.VoiceCommand;
import be.mvdk.iris.app.util.DesktopUtil;

import java.io.File;

/**
 * Created by mvandenk on 17/10/2016.
 */
public class MusicCommandExecutor implements VoiceCommandExecutor {


    @Override
    public void executeVoiceCommand(VoiceCommand voiceCommand) {
        if (Verb.OPEN == voiceCommand.getVerb()) {
            File file = new File("C:\\Program Files (x86)\\Windows Media Player\\wmplayer.exe");
            DesktopUtil.openFile(file);
        }
    }
}
