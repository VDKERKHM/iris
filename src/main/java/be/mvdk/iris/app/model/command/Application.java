package be.mvdk.iris.app.model.command;

import org.springframework.util.StringUtils;

/**
 * Created by mvandenk on 6/10/2016.
 */
public enum Application {
    GOOGLE,
    YOUTUBE,
    MUSIC,
    WORD,
    POWERPOINT,
    MAPS;

    public static Application get(String applicationName) {
        if (!StringUtils.hasText(applicationName)) {
            return null;
        }

        for (Application application : values()) {
            if (application.name().equals(applicationName.toUpperCase())) {
                return application;
            }
        }

        return null;

    }
}
