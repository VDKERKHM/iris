package be.mvdk.iris.app.model;

import be.mvdk.iris.app.model.command.Application;
import be.mvdk.iris.app.model.executor.*;

/**
 * Created by mvandenk on 6/10/2016.
 */
public class ExecutorFactory {

    public static VoiceCommandExecutor getExecutorByApplication(Application app) {
        switch (app) {
            case GOOGLE:
                return new GoogleVoiceCommandExecutor();
            case YOUTUBE:
                return new YouTubeVoiceCommandExecutor();
            case WORD:
                return new OfficeVoiceCommandExecutor();
            case POWERPOINT:
                return new OfficeVoiceCommandExecutor();
            case MAPS:
                return new MapsVoiceCommandExecutor();
            case MUSIC:
                return new MusicCommandExecutor();
            default:
                return new DefaultVoiceCommandExecutor();
        }
    }


}
