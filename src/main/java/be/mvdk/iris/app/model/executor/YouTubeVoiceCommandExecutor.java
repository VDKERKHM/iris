package be.mvdk.iris.app.model.executor;

import be.mvdk.iris.app.model.command.VoiceCommand;
import be.mvdk.iris.app.util.DesktopUtil;
import org.springframework.util.StringUtils;

/**
 * Created by mvandenk on 6/10/2016.
 */
public class YouTubeVoiceCommandExecutor implements VoiceCommandExecutor {

    private final String YOUTUBE_BASE_URL = "http://youtube.com";
    private final String YOUTUBE_SEARCH_URL = "/results?search_query=";

    @Override
    public void executeVoiceCommand(VoiceCommand voiceCommand) {
        switch (voiceCommand.getVerb()) {
            case OPEN:
                open();
                break;
            case SEARCH:
                search(voiceCommand);
                break;
        }
    }

    private void open() {
        DesktopUtil.openBrowser(YOUTUBE_BASE_URL);
    }

    private void search(VoiceCommand command) {
        if (!StringUtils.hasText(command.getCommandString())) {
            return;
        }

        DesktopUtil.openBrowser(YOUTUBE_BASE_URL + YOUTUBE_SEARCH_URL + DesktopUtil.encodeParameterString(command.getCommandString()));
    }
}
