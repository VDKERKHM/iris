package be.mvdk.iris.app;

import be.mvdk.iris.app.views.View;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * Created by mvandenk on 3/10/2016.
 */

@Configuration
@ComponentScan
public class Application {

    public static void main(String[] args) throws IOException {
        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        View.createAndShowGUI();
    }
}
