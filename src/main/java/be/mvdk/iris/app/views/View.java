package be.mvdk.iris.app.views;

import be.mvdk.iris.app.model.VoiceCommandListener;
import be.mvdk.iris.app.model.command.VoiceCommand;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mvandenk on 6/10/2016.
 */
public class View extends JFrame {

    private JTextArea displayArea;
    private VoiceCommandListener voiceCommandListener;
    private static final String NEW_LINE = "\n";


    private View(String title) throws HeadlessException {
        super(title);

        voiceCommandListener = new VoiceCommandListener(this);
        Thread thread = new Thread(voiceCommandListener);
        thread.start();
        //voiceCommandListener.run();
    }

    public static void createAndShowGUI() {
        //Create and set up the window.
        final View frame = new View("Sphynx4 Demo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set up the content pane.
        frame.addComponentsToPane();
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    private void addComponentsToPane() {
        displayArea = new JTextArea();
        displayArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(displayArea);
        scrollPane.setPreferredSize(new Dimension(555, 425));

        getContentPane().add(scrollPane, BorderLayout.CENTER);
    }


    public void addTextFeedback(VoiceCommand command) {
        displayArea.append(command.toString() + NEW_LINE);
    }
}
