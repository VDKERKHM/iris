package be.mvdk.iris.app.util;

import be.mvdk.iris.app.model.command.Application;
import be.mvdk.iris.app.model.command.Verb;
import be.mvdk.iris.app.model.command.VoiceCommand;
import edu.cmu.sphinx.api.SpeechResult;
import org.springframework.util.StringUtils;

/**
 * Created by mvandenk on 17/10/2016.
 */
public class VoiceCommandUtil {


    public static VoiceCommand createVoiceCommand(final SpeechResult speechResult) {

        final String hypothesis = speechResult.getHypothesis();

        if (StringUtils.hasText(hypothesis)) {

            final Verb verb = searchForVerbInHypothesis(hypothesis);

            if (verb == null) {
                return null;
            }

            final Application application = searchForApplicationInHypothesis(hypothesis);

            if (application == null) {
                System.out.println("No application found.");
                return null;
            }

            final VoiceCommand voiceCommand = new VoiceCommand(verb, application, getSearchAble(hypothesis));
            voiceCommand.setIgnored(!activatorUsed(hypothesis));

            return voiceCommand;
        }

        return null;
    }

    private static Verb searchForVerbInHypothesis(final String hypothesis) {
        if (hypothesis.contains("open")) {
            return Verb.OPEN;
        }
        if (hypothesis.contains("search")) {
            return Verb.SEARCH;
        }

        if (hypothesis.contains("exit") || hypothesis.contains("shut down")) {
            return Verb.EXIT;
        }
        return null;
    }

    private static boolean activatorUsed(final String hypothesis) {
        if (hypothesis.contains("hey iris")) {
            return true;
        }
        return false;
    }

    private static Application searchForApplicationInHypothesis(final String hypothesis) {
        if (hypothesis.contains("google")) {
            return Application.GOOGLE;
        }

        if (hypothesis.contains("maps")) {
            return Application.MAPS;
        }

        if (hypothesis.contains("youtube")) {
            return Application.YOUTUBE;
        }

        if (hypothesis.contains("music")) {
            return Application.MUSIC;
        }

        if (hypothesis.contains("powerpoint")) {
            return Application.POWERPOINT;
        }

        if (hypothesis.contains("word")) {
            return Application.WORD;
        }

        return null;
    }

    private static String getSearchAble(final String hypothesis) {
        final int indexOf = hypothesis.indexOf("for");

        if (indexOf > 0) {
            final String command = hypothesis.substring(indexOf + "for".length());
            return StringUtils.trimAllWhitespace(command);
        }

        return hypothesis;
    }
}
