package be.mvdk.iris.app.util;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

/**
 * Created by mvandenk on 17/10/2016.
 */
public class DesktopUtil {

    public static void openBrowser(final String url) {
        Desktop desktop = Desktop.getDesktop();
        try {
            desktop.browse(new URI(url));
        } catch (IOException | URISyntaxException e) {
            System.out.println(e.getMessage());
        }
    }

    public static String encodeParameterString(final String parmeter) {
        try {
            return URLEncoder.encode(parmeter, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    public static void openFile(File file) {
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
